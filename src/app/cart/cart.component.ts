import { SnackbarService } from './../services/snackbar.service';
import { Router } from "@angular/router";
import { OrderItem } from "./../models/OrderItem";
import { OrderService } from "./../services/order.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.css"]
})
export class CartComponent implements OnInit {
  constructor(private orderService: OrderService, private router: Router, 
    private snackbarService: SnackbarService) {}

  cartItems: OrderItem;
  cartValue: number = 0;
  discountValue: number = 0;
  totalPayableAmount: number = 0;
  orderButtonDisabled = false;

  ngOnInit() {
    this.cartItems = this.orderService.itemsInCart;
    console.log(this.cartItems);
    this.cartItems.menu.forEach(x => {
      const value = x.quantity * x.itemPrice;
      this.cartValue = this.cartValue + value;
    });

    if (this.cartItems.discountAdded) {
      this.discountValue = Math.floor(
        this.cartValue * (this.cartItems.discountPercentage / 100)
      );
      console.log(this.discountValue);
    } else {
      this.discountValue = 0;
    }
    this.totalPayableAmount = this.cartValue - this.discountValue;
  }

  onPlaceOrder() {
    this.orderButtonDisabled = true;
    let menuId = [];
    this.cartItems.menu.map(x => {
      const val = {
        menuId: x.menuId,
        quantity: x.quantity
      };
      menuId.push(val);
    });
    const payload = {
      restaurantId: this.cartItems.restaurantId,
      orderAmount: this.totalPayableAmount,
      menuId: menuId
    };
    this.orderService.placeOrder(payload).subscribe(
      data => {
        this.orderService.placedOrderDetails = data;
        this.orderService.itemsInCart = null;
        this.cartItems = null;
        this.router.navigate(["/order/status"]);
      },
      (error: Error) => {
        this.snackbarService.openSnackBar(error.stack, 'Close')
      }
    );
  }
}
