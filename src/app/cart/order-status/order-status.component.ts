import { OrderService } from './../../services/order.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html',
  styleUrls: ['./order-status.component.css']
})
export class OrderStatusComponent implements OnInit {

  constructor(private oderService: OrderService) { }
  placedOrderDetails; 
  ngOnInit() {
    this.placedOrderDetails = this.oderService.placedOrderDetails;
  }

}
