import { SnackbarService } from './../services/snackbar.service';
import { Restaurant } from './../models/Restaurant';
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { SearchService } from "../services/search.service";

@Component({
  selector: "app-restaurant-listing",
  templateUrl: "./restaurant-listing.component.html",
  styleUrls: ["./restaurant-listing.component.css"]
})
export class RestaurantListingComponent implements OnInit {
  restaurants: Restaurant[];
  filteredrestaurants: Restaurant[];
  cityName: String;
  constructor(
    private searchService: SearchService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    const value = this.activatedRoute.snapshot.params["id"];
    this.cityName = value;
    this.searchService.getRestaurantsByLocation(value).subscribe(data => {
      this.restaurants = data;
      this.filteredrestaurants = this.restaurants;
    }, error => this.snackbarService.openSnackBar("Error fetching restaurant details from Db", "Close"));
  }

  radioButtonChange(e) : void{
    this.filteredrestaurants = this.restaurants.filter(x=> x.rating > e); 
  }

  onOrderFood(restaurantId: number) {
    const value = this.activatedRoute.snapshot.params["id"];
    this.router.navigate(["/" + value  + "/" + restaurantId]);
  }
}
