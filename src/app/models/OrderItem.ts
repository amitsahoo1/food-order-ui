import { Menu } from './Menu';

export interface OrderItem {
    restaurantId: number;
    restaurantName: string;
    discountAdded?: boolean;
    discountPercentage?: number;
    menu: Menu[];
}