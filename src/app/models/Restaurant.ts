import { Location } from './Location';
export interface Restaurant {
    restaurantId: number;
    restaurantName: string;
    restaurantIcon: string;
    location: Location;
    rating: number;
    ratingCount: number;
}