export interface Menu {
    menuId: number;
    itemName: string;
    itemDescription?: string;
    itemIcon: string;
    itemPrice: number;
    quantity?:number
}