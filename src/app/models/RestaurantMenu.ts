import { Menu } from './Menu';

export interface RestaurantMenu {
    restaurantId: number;
    restaurantName: string;
    discountAdded?: boolean;
    discountPercentage?: number;
    menus: Menu[];
}