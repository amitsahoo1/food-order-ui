import { AppRoutingModule } from "./app.routing";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { SearchRestaurantComponent } from "./search-restaurant/search-restaurant.component";
import { RestaurantDetailsComponent } from "./restaurant-details/restaurant-details.component";
import { RestaurantListingComponent } from "./restaurant-listing/restaurant-listing.component";
import { HttpClientModule } from "@angular/common/http";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatButtonModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatInputModule,
  MatSnackBarModule
} from "@angular/material";
import { MatSliderModule } from "@angular/material/slider";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CartComponent } from './cart/cart.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NavBarComponent } from "./nav-bar/nav-bar.component";
import { OrderStatusComponent } from './cart/order-status/order-status.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SearchRestaurantComponent,
    RestaurantDetailsComponent,
    RestaurantListingComponent,
    CartComponent,
    OrderStatusComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSliderModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
