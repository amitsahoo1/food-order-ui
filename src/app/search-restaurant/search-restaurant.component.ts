import { SnackbarService } from './../services/snackbar.service';
import { Location } from "./../models/Location";
import { Observable } from "rxjs";
import { startWith, map } from "rxjs/operators";
import { SearchService } from "./../services/search.service";
import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Router } from "@angular/router";


@Component({
  selector: "app-search-restaurant",
  templateUrl: "./search-restaurant.component.html",
  styleUrls: ["./search-restaurant.component.css"]
})
export class SearchRestaurantComponent implements OnInit {
  options: Location[];
  myControl = new FormControl();
  filteredOptions: Observable<Location[]>;

  constructor(private searchService: SearchService, private router: Router, 
    private snackbarService: SnackbarService) {}

  ngOnInit() {
    this.searchService.getAllLocations().subscribe(data => {
      this.options = data;
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith<string | Location>(""),
        map(value => (typeof value === "string" ? value : value.locationName)),
        map(name => (name ? this._filter(name) : this.options.slice()))
      );
    });
  }

  displayFn(user?: Location): string | undefined {
    return user ? user.locationName : undefined;
  }

  private _filter(name: string): Location[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(
      option => option.locationName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  searchRestaurant() {
    if(!this.myControl.value) {
      this.snackbarService.openSnackBar("Please select a city", "Close");
    } else {
      const cityName = this.myControl.value["locationName"];
      this.router.navigate(["/" + cityName]);
    }
  }

  
}
