import { OrderStatusComponent } from './cart/order-status/order-status.component';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router"; // CLI imports router
import { SearchRestaurantComponent } from "./search-restaurant/search-restaurant.component";
import { RestaurantListingComponent } from "./restaurant-listing/restaurant-listing.component";
import { CartComponent } from './cart/cart.component';

const routes: Routes = [
  {path: "cart", component: CartComponent},
  {path: "order/status", component: OrderStatusComponent},
  {path: ":city/:id", component: RestaurantDetailsComponent},
  {path: ":id", component: RestaurantListingComponent},
  { path: "", component: SearchRestaurantComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
