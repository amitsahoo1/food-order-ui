import { OrderItem } from "./../models/OrderItem";
import { Menu } from "./../models/Menu";
import { FormBuilder } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { RestaurantMenu } from "./../models/RestaurantMenu";
import { OrderService } from "./../services/order.service";
import { SearchService } from "./../services/search.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-restaurant-details",
  templateUrl: "./restaurant-details.component.html",
  styleUrls: ["./restaurant-details.component.css"]
})
export class RestaurantDetailsComponent implements OnInit {
  restaurantMenu: RestaurantMenu;
  filteredRestaurantMenu: Menu[];
  menuIdInCart: Set<number> = new Set<number>();
  priceFilterForm: FormGroup;

  minPrice: number;
  maxPrice: number;
  constructor(
    private route: ActivatedRoute,
    private searchService: SearchService,
    private orderService: OrderService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.priceFilterForm = this.fb.group({
      minPrice: [0],
      maxPrice: [9999]
    });
    const restaurantId = this.route.snapshot.params["id"];
    this.searchService
      .getRestaurantByRestaurantId(restaurantId)
      .subscribe(data => {
        this.restaurantMenu = data;
        this.filteredRestaurantMenu = this.restaurantMenu.menus;
        console.log(this.restaurantMenu);
      });
  }

  onAddToCart(menu) {
    if (this.orderService.itemsInCart) {
      if (
        this.orderService.itemsInCart.restaurantId !=
        this.restaurantMenu.restaurantId
      ) {
        if (confirm("Do you want to clear")) {
          const orderItem: OrderItem = {
            restaurantId: this.restaurantMenu.restaurantId,
            restaurantName: this.restaurantMenu.restaurantName,
            discountAdded: this.restaurantMenu.discountAdded,
            discountPercentage: this.restaurantMenu.discountPercentage,
            menu: [
              {
                menuId: menu.menuId,
                itemName: menu.itemName,
                itemIcon: menu.itemIcon,
                itemPrice: menu.itemPrice,
                quantity: 1
              }
            ]
          };
          this.orderService.itemsInCart = orderItem;
          this.menuIdInCart.add(menu.menuId);
        }
      } else {
        this.orderService.itemsInCart.menu.push({
          menuId: menu.menuId,
          itemName: menu.itemName,
          itemIcon: menu.itemIcon,
          itemPrice: menu.itemPrice,
          quantity: 1
        });
        this.menuIdInCart.add(menu.menuId);
      }
    } else {
      const orderItem: OrderItem = {
        restaurantId: this.restaurantMenu.restaurantId,
        restaurantName: this.restaurantMenu.restaurantName,
        discountAdded: this.restaurantMenu.discountAdded,
        discountPercentage: this.restaurantMenu.discountPercentage,
        menu: [
          {
            menuId: menu.menuId,
            itemName: menu.itemName,
            itemIcon: menu.itemIcon,
            itemPrice: menu.itemPrice,
            quantity: 1
          }
        ]
      };
      this.orderService.itemsInCart = orderItem;
      this.menuIdInCart.add(menu.menuId);
    }
  }

  onPriceFilterClick() {
    const minPrice = this.priceFilterForm.get("minPrice").value;
    const maxPrice = this.priceFilterForm.get("maxPrice").value;
    this.filteredRestaurantMenu = this.restaurantMenu.menus.filter(
      x => x.itemPrice >= minPrice && x.itemPrice <= maxPrice
    );
    console.log(this.filteredRestaurantMenu);
  }

  isAddedToCart(menu): boolean {
    return this.menuIdInCart.has(menu.menuId) ? true : false;
  }

  getQuantity(menuId: number): number {
    return this.orderService.itemsInCart.menu.find(x => x.menuId == menuId)
      .quantity;
  }

  onAddingQuantity(menu) {
    const index = this.orderService.itemsInCart.menu.findIndex(
      x => x.menuId == menu.menuId
    );
    this.orderService.itemsInCart.menu[index].quantity =
      this.orderService.itemsInCart.menu[index].quantity + 1;
  }

  onRemovingQuantity(menu) {
    const index = this.orderService.itemsInCart.menu.findIndex(
      x => x.menuId == menu.menuId
    );
    this.orderService.itemsInCart.menu[index].quantity =
      this.orderService.itemsInCart.menu[index].quantity - 1;
  }
}
