import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { OrderItem } from './../models/OrderItem';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  public itemsInCart:OrderItem;
  
  public placedOrderDetails;

  constructor(private httpClient: HttpClient) { }

  placeOrder(payload: any): Observable<any> {
    return this.httpClient.post('http://localhost:8080/order/createOrder', payload);
  }
}
