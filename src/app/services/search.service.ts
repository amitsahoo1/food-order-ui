import { Restaurant } from './../models/Restaurant';
import { Location } from './../models/Location';
import { RestaurantMenu } from './../models/RestaurantMenu';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  getAllLocations(): Observable<Location[]> {
    return this.http.get<Location[]>('http://localhost:8080/location');
  }

  getRestaurantsByLocation(locationName: string) : Observable<Restaurant[]> {
    return this.http.get<Restaurant[]>('http://localhost:8080/restaurant/location?loc=' + locationName);
  }

  getRestaurantByRestaurantId(restaurantId: number) : Observable<RestaurantMenu> {
    return this.http.get<RestaurantMenu>('http://localhost:8080/restaurant?restaurantId=' + restaurantId);
  }
}
